﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter value of circles (2..10):");
            var n = Convert.ToInt32(Console.ReadLine());
            if (n < 2 && n > 10)
            {
                Console.WriteLine("This number < 2 or > 10");
                return;
            }

            var x = new double[n];
            var y = new double[n];
            var r = new double[n];

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Enter circle in format(x y r):");
                var st = Console.ReadLine();
                var t = st.Split(' ');
                x[i] = Convert.ToDouble(t[0]);
                y[i] = Convert.ToDouble(t[1]);
                r[i] = Convert.ToDouble(t[2]);
            }

            int ans = 0;
            for (int i = 0; i < n; i++)
            {
                int ans_prom = 0;
                for (int q = 0; q < n; q++)
                {
                    if (q == i)
                        continue;
                    if (Math.Sqrt((x[q] - x[i]) * (x[q] - x[i]) + (y[q] - y[i]) * (y[q] - y[i])) == r[i])
                        ans_prom++;
                }
                if (ans < ans_prom)
                    ans = ans_prom;
            }
            Console.WriteLine(ans);
            Console.ReadLine();
        }
    
    }
}
